<?php

namespace MyLocalPitch\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Slot
 *
 * @ORM\Table(name="slot")
 * @ORM\Entity(repositoryClass="MyLocalPitch\ApiBundle\Repository\SlotRepository")
 * @ExclusionPolicy("all")
 */
class Slot
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="starts", type="datetime")
     * @Expose()
     */
    private $starts;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ends", type="datetime")
     * @Expose()
     */
    private $ends;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=6, scale=2)
     * @Expose()
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=255)
     * @Expose()
     */
    private $currency;

    /**
     * @var bool
     *
     * @ORM\Column(name="available", type="boolean")
     * @Expose()
     */
    private $available;

    /**
     * @ORM\ManyToOne(targetEntity="MyLocalPitch\ApiBundle\Entity\Pitch", inversedBy="slots")
     * @ORM\JoinColumn(name="pitchId", referencedColumnName="id")
     */
    protected $pitch;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set starts
     *
     * @param \DateTime $starts
     *
     * @return Slot
     */
    public function setStarts($starts)
    {
        $this->starts = $starts;

        return $this;
    }

    /**
     * Get starts
     *
     * @return \DateTime
     */
    public function getStarts()
    {
        return $this->starts;
    }

    /**
     * Set ends
     *
     * @param \DateTime $ends
     *
     * @return Slot
     */
    public function setEnds($ends)
    {
        $this->ends = $ends;

        return $this;
    }

    /**
     * Get ends
     *
     * @return \DateTime
     */
    public function getEnds()
    {
        return $this->ends;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Slot
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return Slot
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set available
     *
     * @param boolean $available
     *
     * @return Slot
     */
    public function setAvailable($available)
    {
        $this->available = $available;

        return $this;
    }

    /**
     * Get available
     *
     * @return bool
     */
    public function getAvailable()
    {
        return $this->available;
    }

    /**
     * Set pitch
     *
     * @param \MyLocalPitch\ApiBundle\Entity\Pitch $pitch
     *
     * @return Slot
     */
    public function setPitch(\MyLocalPitch\ApiBundle\Entity\Pitch $pitch = null)
    {
        $this->pitch = $pitch;

        return $this;
    }

    /**
     * Get pitch
     *
     * @return \MyLocalPitch\ApiBundle\Entity\Pitch
     */
    public function getPitch()
    {
        return $this->pitch;
    }
}
