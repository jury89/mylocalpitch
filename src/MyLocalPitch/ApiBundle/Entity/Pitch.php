<?php

namespace MyLocalPitch\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Pitch
 *
 * @ORM\Table(name="pitch")
 * @ORM\Entity(repositoryClass="MyLocalPitch\ApiBundle\Repository\PitchRepository")
 * @ExclusionPolicy("all")
 */
class Pitch
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text")
     * @Expose
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="sport", type="string", length=255)
     * @Expose
     */
    private $sport;

    /**
     * @ORM\OneToMany(targetEntity="MyLocalPitch\ApiBundle\Entity\Slot", mappedBy="pitch")
     */
    private $slots;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Pitch
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sport
     *
     * @param string $sport
     *
     * @return Pitch
     */
    public function setSport($sport)
    {
        $this->sport = $sport;

        return $this;
    }

    /**
     * Get sport
     *
     * @return string
     */
    public function getSport()
    {
        return $this->sport;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->slots = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add slot
     *
     * @param \MyLocalPitch\ApiBundle\Entity\Slot $slot
     *
     * @return Pitch
     */
    public function addSlot(\MyLocalPitch\ApiBundle\Entity\Slot $slot)
    {
        $this->slots[] = $slot;

        return $this;
    }

    /**
     * Remove slot
     *
     * @param \MyLocalPitch\ApiBundle\Entity\Slot $slot
     */
    public function removeSlot(\MyLocalPitch\ApiBundle\Entity\Slot $slot)
    {
        $this->slots->removeElement($slot);
    }

    /**
     * Get slots
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSlots()
    {
        return $this->slots;
    }
}
