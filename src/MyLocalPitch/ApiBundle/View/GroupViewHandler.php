<?php

namespace MyLocalPitch\ApiBundle\View;

use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandler;
use Symfony\Component\HttpFoundation\Request;

class GroupViewHandler
{


    public function createResponse(ViewHandler $handler, View $view, Request $request, $type)
    {
        $data = array(
            'meta' => array('total_items' => count($view->getData()))
        );

        foreach ($view->getData() as $pitch) {
            $data['data'][] = array(
                'type' => $type,
                'id' => $pitch->getId(),
                'attributes' => $pitch
            );
        }

        $view->setData($data);

        return $handler->createResponse($view, $request, 'json');
    }
}