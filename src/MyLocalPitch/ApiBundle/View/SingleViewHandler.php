<?php

namespace MyLocalPitch\ApiBundle\View;

use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandler;
use Symfony\Component\HttpFoundation\Request;

class SingleViewHandler
{
    public function createResponse(ViewHandler $handler, View $view, Request $request)
    {
        $pitch = $view->getData();

        $data = array(
            'data' => array(
                'type' => 'pitches',
                'id' => $pitch->getId(),
                'attributes' => $pitch
            )
        );

        $view->setData($data);

        return $handler->createResponse($view, $request, 'json', 'pitches');
    }
}