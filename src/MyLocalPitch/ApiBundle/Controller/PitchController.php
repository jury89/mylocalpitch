<?php

namespace MyLocalPitch\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use MyLocalPitch\ApiBundle\Entity\Pitch;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class PitchController extends FOSRestController
{

    /**
     * @ApiDoc()
     * @return array Pitch
     */
    public function getPitchesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $pitches = $em->getRepository('MyLocalPitchApiBundle:Pitch')->findAll();

        $view = $this->view($pitches, 200);

        $handler = $this->get('my_local_pitch_api.group_handler');

        return $handler->createResponse($this->getViewHandler(), $view, $request, 'pitches');
    }

    /**
     * @ParamConverter()
     * @ApiDoc()
     * @param Pitch $pitch
     * @return Pitch
     */
    public function getPitchAction(Request $request, Pitch $pitch)
    {
        $view = $this->view($pitch, 200);

        $handler = $this->get('my_local_pitch_api.single_handler');

        return $handler->createResponse($this->getViewHandler(), $view, $request, 'pitches');
    }
}
