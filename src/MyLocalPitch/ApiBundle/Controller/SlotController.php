<?php

namespace MyLocalPitch\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use MyLocalPitch\ApiBundle\Entity\Pitch;
use MyLocalPitch\ApiBundle\Entity\Slot;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class SlotController extends FOSRestController
{

    /**
     * @ParamConverter()
     * @ApiDoc()
     * @return array Slot
     */
    public function getSlotsAction(Request $request, Pitch $pitch)
    {
        $em = $this->getDoctrine()->getManager();

        $slots = $em->getRepository('MyLocalPitchApiBundle:Slot')->findBy(array(
            'pitch' => $pitch
        ));

        $view = $this->view($slots, 200);

        $handler = $this->get('my_local_pitch_api.group_handler');

        return $handler->createResponse($this->getViewHandler(), $view, $request, 'slots');
    }

    /**
     * @ParamConverter()
     * @ApiDoc()
     * @return array Slot
     */
    public function postSlotsAction(Request $request, Pitch $pitch)
    {
        $arrData = $request->get('data');
        $em = $this->getDoctrine()->getManager();

        foreach ($arrData as $data) {
            if ($data['type'] == 'slots') {
                $attr = $data['attributes'];
                $slot = new Slot();

                try {
                    $slot->setStarts(new \DateTime($attr['starts']));
                    $slot->setEnds(new \DateTime($attr['ends']));
                    $slot->setPrice($attr['price']);
                    $slot->setCurrency($attr['currency']);
                    $slot->setAvailable($attr['available']);

                    $slot->setPitch($pitch);

                    $em->persist($slot);
                } catch (\Exception $e) {
                    $view = $this->view('Request malformed.', 500);
                    return $this->handleView($view);
                }
            }
        }

        try {
            $em->flush();
            $view = $this->view(null, 201);
            return $this->handleView($view);
        } catch (\Exception $e) {
            $view = $this->view('Internal error', 500);
            return $this->handleView($view);
        }
    }
}
