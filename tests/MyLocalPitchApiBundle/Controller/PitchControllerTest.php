<?php

namespace MyLocalPitch\ApiBundle\Tests\Controller;

use Lakion\ApiTestCase\JsonApiTestCase;

class DefaultControllerTest extends JsonApiTestCase
{

    public function testGetPitches()
    {
        $this->loadFixturesFromFile('pitches.yml');

        $this->client->request('GET', '/pitches');

        $response = $this->client->getResponse();

        $this->assertResponseCode($response, 200);
        $this->assertResponse($response, 'get_pitches');

        $this->purgeDatabase();
    }

    public function testGetPitch()
    {
        $this->loadFixturesFromFile('pitches.yml');

        $em = $this->get('doctrine')->getManager();
        $pitches = $em->getRepository('MyLocalPitchApiBundle:Pitch')->findAll();

        $this->client->request('GET', '/pitches/' . $pitches[0]->getId());

        $response = $this->client->getResponse();

        $this->assertResponseCode($response, 200);
        $this->assertResponse($response, 'get_pitch');

        $this->purgeDatabase();
    }

    public function testGetPitchesSlots()
    {
        $this->loadFixturesFromFile('pitches.yml');
        $this->loadFixturesFromFile('slots.yml');

        $em = $this->get('doctrine')->getManager();
        $pitches = $em->getRepository('MyLocalPitchApiBundle:Pitch')->findAll();

        $this->client->request('GET', '/pitches/' . $pitches[0]->getId() . '/slots');

        $response = $this->client->getResponse();

        $this->assertResponseCode($response, 200);
        $this->assertResponse($response, 'get_slots');

        $this->purgeDatabase();
    }

    public function testPostPitchesSlots()
    {
        $this->loadFixturesFromFile('pitches.yml');

        $em = $this->get('doctrine')->getManager();
        $pitches = $em->getRepository('MyLocalPitchApiBundle:Pitch')->findAll();

        $data = array(
            "data" => array(
                array(
                    "type" => "slots",
                    "id" => null,
                    "attributes" => array(
                        "starts" => "2016-11-04T10:00:00+00:00",
                        "ends" => "2016-11-04T11:00:00+00:00",
                        "price" => "32.00",
                        "currency" => "GBP",
                        "available" => true
                    )
                )
            )
        );

        $this->client->request('POST', '/pitches/' . $pitches[0]->getId() . '/slots', $data);

        $response = $this->client->getResponse();

        $this->assertResponseCode($response, 201);

        $this->purgeDatabase();
    }

    public function testPostPitchesSlotsRequestMalformed()
    {
        $this->loadFixturesFromFile('pitches.yml');

        $em = $this->get('doctrine')->getManager();
        $pitches = $em->getRepository('MyLocalPitchApiBundle:Pitch')->findAll();

        $data = array(
            "data" => array(
                array(
                    "type" => "slots",
                    "id" => null,
                    "attributes" => array()
                )
            )
        );

        $this->client->request('POST', '/pitches/' . $pitches[0]->getId() . '/slots', $data);

        $response = $this->client->getResponse();

        $this->assertResponseCode($response, 500);
        $this->assertEquals('"Request malformed."', $response->getContent());

        $this->purgeDatabase();
    }

    /*
     * more tests that could be written for POST /pitches/{id}/slots:
     *
     * - don't set data
     * - set data empty
     * - don't set data.type
     * - set data.type = empty
     * - set stars/ends in different formats
     * - set price in different formats
     */

}
